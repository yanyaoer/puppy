import sys
import logging
import json
import datetime
import decimal
from urllib.parse import parse_qs, urlencode
from raven.contrib.tornado import SentryMixin
from sqlalchemy import desc
from sqlalchemy.exc import SQLAlchemyError
from tornado import locale
from tornado.web import HTTPError
from tornado.web import RequestHandler
from tornado.options import options
from pager import Paginator


class Base(SentryMixin, RequestHandler):
  def initialize(self, **kwargs):
    self.ui.update({
      'paragraph': lambda raw: "<br /><br />".join(raw.splitlines() if raw else ""),
      'to_link': lambda obj: obj.linkify() if hasattr(obj, 'linkify') else obj,
      #'lang': lambda: json.dumps(self.locale.get('zh_CN').translations.get('unknown', {})),
      #'year': lambda: datetime.datetime.now().year,
      #'is_thumb': lambda oid: re.findall('/\d+x\d+$', oid)
      #'user': lambda : self.jsondumps(self.current_user)
    })
    if hasattr(self, 'set_form'):
      self.set_form()

  @property
  def db(self):
    return self.application.db

  def on_finish(self):
    self.db.close()

  def get_user_locale(self):
    return locale.get('zh_CN')

  def get_current_user(self):
    return self.get_secure_cookie("root")

  def permission(self):
    if options.debug or self.current_user:
      return True
    url = ['choose', 'login']
    for x in url:
      if self.request.uri.startswith('/' + x):
        return True
        break
    return False

  def prepare(self):
    if not self.permission():
      self.redirect("/login")
      return

    method = self.get_argument('mtd', '')
    if method.upper() in self.SUPPORTED_METHODS:
      return getattr(self, method.lower())(*self.path_args, **self.path_kwargs)

  def get_page_data(self, query):
    page_size = self.application.settings.get("page_size", 10)
    page = int(self.get_argument("page", 1))
    offset = (page - 1) * page_size
    result = query.limit(page_size).offset(offset)
    page_data = Paginator(self.get_page_url, page, query.count(), page_size)
    page_data.result = result
    return page_data

  def get_page_url(self, page, form_id=None):
    if form_id:
      return "javascript:goto_page('%s',%s);" % (form_id.strip(), page)
    path = self.request.path
    query = self.request.query
    qdict = parse_qs(query)
    for k, v in qdict.items():
      if isinstance(v, list):
        qdict[k] = v and v[0] or ''
    qdict['page'] = page
    return path + '?' + urlencode(qdict)

  def jsondump(self, obj, output=True):
    def json_default(raw):
      if hasattr(raw, 'to_dict'):
        return raw.to_dict()
      if isinstance(raw, datetime.datetime):
        return raw.strftime('%Y-%m-%d %H:%M:%S')
      if isinstance(raw, decimal.Decimal):
        return float(raw)
    json_str = json.dumps(obj, default=json_default)
    if output:
      self.set_header('Content-Type', 'application/json')
      self.write(json_str)
    else:
      return json_str

  def put(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      d = self.db.query(self.model).filter_by(id=id).first()
      f.populate_obj(d)
      d.save()
      self.redirect('/' + self.ft + '/' + id)
    else:
      self.set_status(400)
      self.write(f.errors)

  def post(self, id=None):
    f = self.form(FormWrapper(self))
    if f.validate():
      self.model(**f.data).save()
      self.redirect('/' + self.ft + '/')
    else:
      self.set_status(400)
      self.write(f.errors)

  def delete(self, id):
    d = self.db.query(self.model).filter_by(id=id).first()
    if d:
      try:
        d.delete()
        self.redirect(self.request.headers.get('Referer', '/' + self.ft + '/'))
      except SQLAlchemyError as e:
        self.db.rollback()
        logging.exception(e)
        self.send_error(500, exc_info=sys.exc_info())
        return
    else:
      raise HTTPError(404)

  def get(self, id=None):
    q = self.db.query(self.model).order_by(desc(self.model.id))
    if id:
      q = q.filter_by(id=id)
    self.render('common.html', pager=self.get_page_data(q))


class IndexHandler(Base):
  def get(self, args):
    self.render('index.html')


class FormWrapper(object):
  def __init__(self, handler):
    self.handler = handler

  def __iter__(self):
    return iter(self.handler.request.arguments)

  def __len__(self):
    return len(self.handler.request.arguments)

  def __contains__(self, name):
    return (name in self.handler.request.arguments)

  def getlist(self, name):
    return self.handler.get_arguments(name, strip=False)
