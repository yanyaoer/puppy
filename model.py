from datetime import datetime
from tornado.web import HTTPError
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import scoped_session, sessionmaker, Query, relationship
from sqlalchemy.ext.declarative import as_declarative, declared_attr
from sqlalchemy import create_engine, Integer, Column, DateTime, ForeignKey
from sqlalchemy.dialects.mysql import INTEGER, TINYINT, VARCHAR, TEXT
import setting


__all__ = [
  'db', 'Author', 'Tag', 'Book', 'History', 'Comment', 'Parent', 'User'
]


class BaseQuery(Query):
  def get_or_404(self, ident):
    rv = self.get(ident)
    if rv is None:
      raise HTTPError(404)
    return rv

  def first_or_404(self):
    rv = self.first()
    if rv is None:
      raise HTTPError(404)
    return rv


base = automap_base()

@as_declarative()
class Base(base):

  @declared_attr
  def __tablename__(cls):
      return cls.__name__.lower()

  __table_args__  = {'mysql_engine': 'InnoDB'}
  __mapper_args__ = {'always_refresh': True}
  query_class     = BaseQuery

  id      = Column(Integer, primary_key=True)
  created = Column(DateTime, default=datetime.utcnow)
  updated = Column(DateTime,
                   default  = datetime.utcnow,
                   onupdate = datetime.utcnow,
                   index    = True)

  def save(self):
    db.add(self)
    db.commit()
    return self

  def delete(self):
    db.delete(self)
    db.commit()
    return self

  def to_dict(self):
    return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Author(Base):
  __tablename__ = 'authors2'

  name    = Column(VARCHAR(50), nullable=False)
  country = Column(VARCHAR(20))
  prize   = Column(TEXT)

  def __repr__(self):
    return '<Author %r>' % self.name

  def linkify(self):
    return '<a href="/author/%r">%r</a>' % (self.id, self.name)


class Book(Base):
  __tablename__ = 'books4'

  def __repr__(self):
    return '<Book4 %r>' % (self.name)

  def linkify(self):
    return '<a href="/book/%r"> %r</a>' % (self.id, self.name)


class List(Base):
  __tablename__ = 'lists'
  status  = Column(Integer, default=0)

  def __repr__(self):
    return '<List %r>' % (self.name)

  def linkify(self):
    return '<a href="/list/%r"> %r</a>' % (self.id, self.name)


class User(Base):
  __tablename__ = 'users2'
  """
  `card_id` varchar(20) DEFAULT NULL,
  `father_id` int(11) DEFAULT NULL,
  `mother_id` int(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL COMMENT '0-F;1-M',
  `name` varchar(20) DEFAULT '' COMMENT '姓名',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '登录名',
  """
  username  = Column(VARCHAR(20), nullable=False)
  card_id   = Column(VARCHAR(20))
  father_id = Column(INTEGER(11))
  mother_id = Column(INTEGER(11))
  birthday  = Column(DateTime)
  gender    = Column(TINYINT(1))
  name      = Column(VARCHAR(20))

  def __repr__(self):
    return '<User %r>' % (self.name)

  def linkify(self):
    return '<a href="/user/%r">%r</a>' % (self.id, self.name)


class Parent(Base):
  __tablename__ = 'parents2'
  """
  `name` varchar(20) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `address` text,
  `job` text,
  `marriage` int(1) DEFAULT NULL COMMENT '0-single;1-married;2-divorced;3-widowed',
  `phone` varchar(20) DEFAULT NULL,
  `qq` varchar(20) DEFAULT NULL,
  `wechat` varchar(50) DEFAULT NULL,
  `concept` text COMMENT '教育理念',
  """
  name     = Column(VARCHAR(20), nullable=False)
  gender   = Column(TINYINT(1))
  age      = Column(INTEGER(3))
  marriage = Column(TINYINT(1))
  phone    = Column(VARCHAR(20))
  qq       = Column(VARCHAR(20))
  wechat   = Column(VARCHAR(50))
  address  = Column(TEXT)
  job      = Column(TEXT)
  concept  = Column(TEXT)


class History(Base):
  __tablename__ = 'history2'
  """
  `user_id` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `borrow_time` timestamp NULL DEFAULT NULL,
  `return_time` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `broken_desc` text,
  `broken_level` int(11) DEFAULT NULL COMMENT '0-5',
  `broken_money` text,
  """
  user_id      = Column(INTEGER(11), ForeignKey('users2.id'))
  user         = relationship("User")
  book_id      = Column(INTEGER(11), ForeignKey('books4.id'))
  book         = relationship("Book")
  #comment_id   = Column(INTEGER(11), ForeignKey('comments2.id'))
  #comment      = relationship("Comment")
  borrow_time  = Column(DateTime)
  return_time  = Column(DateTime)
  broken_level = Column(INTEGER(11))
  broken_desc  = Column(TEXT)
  broken_money = Column(TEXT)

  def __repr__(self):
    return '<User %r Book %r>' % (self.user.name, self.book.name)


class Comment(Base):
  __tablename__ = 'comments2'
  """
  `source` int(11) DEFAULT NULL COMMENT '0-本图书馆借书评论,1-douban,...',
  `rank` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL COMMENT 'comment username',
  `date` date DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `content` text,
  """
  source   = Column(INTEGER(11))
  rank     = Column(INTEGER(11))
  username = Column(VARCHAR(50))
  date     = Column(DateTime)
  book_id  = Column(INTEGER(11))
  #book_id  = Column(INTEGER(11), ForeignKey('books4.id'))
  #book     = relationship("Book")
  content  = Column(TEXT)

  def __repr__(self):
    return '<Comment %r: %r>' % (self.username, self.content)

  def linkify(self):
    return '<a href="/comment/%r">%r say: %r</a>' % (self.id, self.username, self.content)


class Tag(Base):
  __tablename__ = 'tags2'

  name      = Column(VARCHAR(50), nullable=False)
  type      = Column(INTEGER(11))
  parent_id = Column(INTEGER(11))

  def __repr__(self):
    return '<Tag %r>' % self.name


class TagBookLink(Base):
  __tablename__ = "tag_book_link"

  #id      = Column(Integer, primary_key=True)
  tag_id  = Column(Integer, ForeignKey("tags2.id"))
  tag     = relationship("Tag")
  book_id = Column(Integer, ForeignKey("books4.id"))
  book    = relationship("Book")

  def __repr__(self):
    return '<Tag-%r Book-%r>' % (self.tag_id, self.book_id)


engine = create_engine(setting.mysql, pool_recycle=3600)
Base.prepare(engine, reflect=True)
#Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
db = scoped_session(sessionmaker(bind=engine))
Base.query = db.query_property()
