from wtforms.fields import SelectField
from wtforms.form import Form
from wtforms.ext.sqlalchemy.orm import model_form
from sqlalchemy import desc
from model import List, Book
from handler.base import Base


__all__ = ["ListHandler"]


class ListHandler(Base):
  def set_form(self):
    self.model = List
    self.ft = 'booklist'
    self.attrs = ['id', 'name', 'mobile', 'address', 'booklist', 'created', 'status', 'comment']
    form = model_form(List, base_class=Form, db_session=self.db,
                           exclude=['created', 'updated', 'name', 'mobile', 'address', 'booklist'],
                           )
    self.choices = ['未审核', '已审核', '已领取', '已取消']
    form.status = SelectField(coerce=int, choices=list(enumerate(self.choices)))
    self.form = form

  def get(self, id=None):
    status = int(self.get_argument('status', -1))
    q = self.db.query(self.model).order_by(desc(self.model.id))
    if status >= 0:
      q = q.filter_by(status=status)
    print(status, q, q.all())
    if id:
      res = q.filter_by(id=id).first()
      book = self.db.query(Book)\
                    .filter(Book.id.in_(res.booklist.split(',')))\
                    .group_by(Book.id).all()
      self.render('list_detail.html',
                  book=book,
                  result=res)
    else:
      self.render('list.html', pager=self.get_page_data(q))
