from sqlalchemy import desc
from model import Book, Tag, TagBookLink
from handler.base import Base
import setting


class LoginHandler(Base):
  def get(self):
    html = """<html>
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <body><form action="/login" method="post">
    Name: <input type="text" name="name">
    <br />
    Secret: <input type="password" name="secret">
    <br />
    <input type="submit" value="Sign in">
    </form></body></html>"""
    self.write(html)

  def post(self):
    name = self.get_argument("name")
    secret = self.get_argument("secret")
    if (name, secret) == setting.auth:
      self.set_secure_cookie("root", "enable")
    else:
      self.clear_cookie("root")
    self.redirect("/")


class ApiHandler(Base):
  def get(self, type, *args):
    attr = 'get_' + type
    if hasattr(self, attr):
      getattr(self, attr)(*args)

  def get_tag(self):
    q = self.db.query(Tag).order_by(desc(Tag.created))
    qt = self.get_argument('type', None)
    choices = ['book', 'author', 'parent', 'user']
    if qt and qt in choices:
      q = q.filter_by(type=choices.index(qt) + 1)
    self.jsondump(q.all())

  #Deprecated
  def get_search(self):
    kw = self.get_argument('q', None)
    q = self.db.query(Book).order_by(desc(Book.created))\
        .filter(Book.name.like('%{0}%'.format(kw)))
    self.jsondump(q.all())

  def post(self, *args):
    mtd = self.get_argument('mtd', None)
    tag = int(self.get_argument('tag', None))
    book = int(self.get_argument('book', None))
    if tag and book:
      if mtd == 'add':
        TagBookLink(tag_id=tag, book_id=book).save()
      elif mtd == 'del':
        self.db.query(TagBookLink).filter_by(tag_id=tag, book_id=book).first().delete()
