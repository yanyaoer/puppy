from wtforms import validators
from wtforms.form import Form
#from wtforms.fields import SelectMultipleField
from wtforms.ext.sqlalchemy.orm import model_form
from model import Book, TagBookLink
from handler.base import Base
from sqlalchemy import desc


__all__ = ["BookHandler"]


class BookHandler(Base):
  def set_form(self):
    args = {k: {'validators': [validators.Required()]} for k in
             ['name', 'author', 'page_count' 'cover', 'broken',
              'price', 'publisher', 'publish_date', 'isbn']}
              #'age_range', 'price', 'publisher', 'publish_date', 'isbn']}

    form = model_form(Book, base_class=Form, db_session=self.db,
                      exclude=['created', 'updated', 'tag',
                               'tagbooklink_collection', 'history_collection'],
                      field_args=args)

    #form.age_range = SelectMultipleField('Book.age_range',
                                         #coerce=int,
                                         #choices=[(i, i) for i in range(1, 13)])
    #form.status = SelectField(coerce=int,
        #choices=list(enumerate(['在架', '已借出', '丢失', '预告'], start=1)),
      #)
    #form.publish_date = DateField(format='%Y-%m-%d')

    self.model = Book
    self.ft = 'book'
    self.attrs = ['id', 'name', 'author', 'language']
    self.form = form

  def get(self, id):
    kw = self.get_argument('q', None)
    q = self.db.query(Book).order_by(desc(Book.id))
    if kw:
      q = q.filter(Book.name.like('%{0}%'.format(kw)))
    if id:
      q = q.filter_by(id=id)
      tags = self.db.query(TagBookLink).filter_by(book_id=id).all()
      self.render('book.html',
                  tags=tags,
                  result=q.first())
    else:
      self.render('common.html', pager=self.get_page_data(q))
