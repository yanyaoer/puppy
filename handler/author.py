from wtforms import validators
from wtforms.form import Form
from wtforms.ext.sqlalchemy.orm import model_form
from sqlalchemy import desc
from model import Author, Book
from handler.base import Base


__all__ = ["AuthorHandler"]


class AuthorHandler(Base):
  def set_form(self):
    self.model = Author
    self.ft = 'author'
    self.attrs = ['id', 'name', 'prize']
    self.form = model_form(Author, base_class=Form, db_session=self.db,
                           exclude=['created', 'updated', 'book'],
                           field_args={
                             'name': {'validators': [
                               validators.Required(),
                               validators.Length(min=5)
                             ]},
                           })

  def get(self, id):
    q = self.db.query(self.model).order_by(desc(self.model.id))
    if id:
      q = q.filter_by(id=id)
      books = self.db.query(Book).filter_by(author_id=id).order_by(desc(Book.id))
      print(books.all)
      self.render('author.html',
                  #tags=tags,
                  pager=self.get_page_data(books),
                  result=q.first())
    else:
      self.render('common.html', pager=self.get_page_data(q))
