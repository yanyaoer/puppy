from tornado.web import UIModule


class Form(UIModule):
  def render(self, form, prefix=''):
    return self.render_string('mod/form.html', form=form, prefix=prefix)

