from tornado.web import HTTPError
from wtforms.form import Form
from wtforms.ext.sqlalchemy.orm import model_form
from wtforms.fields import SelectField
from model import Tag
from handler.base import Base, FormWrapper
from sqlalchemy import desc


__all__ = ["TagHandler"]


class TagHandler(Base):
  def set_form(self):
    self.form = model_form(Tag, base_class=Form, db_session=self.db,
                           exclude=['created', 'updated'])
    choices = ['book', 'author', 'parent', 'user']
    self.form.type = SelectField(coerce=int,
                                 choices=list(enumerate(choices, start=1)))

  def put(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      d = self.db.query(Tag).filter_by(id=id).first()
      f.populate_obj(d)
      d.save()
      self.redirect('/tag/' + id)
    else:
      self.set_status(400)
      self.write(f.errors)

  def post(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      Tag(**f.data).save()
      self.redirect('/tag/')
    else:
      self.set_status(400)
      self.write(f.errors)

  def delete(self, id):
    d = self.db.query(Tag).filter_by(id=id).first()
    if d:
      d.delete()
      self.redirect('/tag/')
    else:
      raise HTTPError(404)

  def get(self, id):
    q = self.db.query(Tag).order_by(desc(Tag.created))
    if id:
      q = q.filter_by(id=id)
    #self.render('tag.html', pager=self.get_page_data(q))
    self.render('form.html',
                ft='tag',
                attrs = ['id', 'name', 'type', 'parent_id'],
                pager=self.get_page_data(q))
