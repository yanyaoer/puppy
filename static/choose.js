function url_pipe(str) {
  var href = window.location.href
  $.each(str.split('&'), function(i, d) {
    var p = ~href.indexOf('?') ? '&' : '?'
    var query = d.replace(/(\d|,)+/, '')
    if (~href.indexOf(query)) {
      var reg = new RegExp(query + '\(\\d\|,\)+', 'g')
      href = href.replace(reg, '').replace(/&+/, '&')
    }
    href += p + d
  })
  window.location.href = href
}

$(function(){

  $('.entry-age span').click(function(e) {
    $(this).toggleClass('active')
  })
  $('.entry-tag-list span').click(function(e) {
    $(this).toggleClass('active')
  })
  $('[data-step=3] .entry-book-list tbody tr').click(function(e) {
    $(this).toggleClass('active')
  })

  $('[data-step=1] button').click(function(e) {
    var list = []
    $('.entry-age .active').each(function(i, el){
      list.push($(el).data('age'))
    })
    if (list.length) {
      url_pipe('age=' + list.join(',') + '&step=2')
    } else {
      alert($('[data-step=1] .entry-title').text())
    }
  })

  $('[data-step=2] button').click(function(e) {
    var list = []
    $('.entry-tag-list .active').each(function(i, el){
      list.push($(el).data('tid'))
    })
    if ($(this).hasClass('skip')) {
      url_pipe('tag=&step=3')
      return
    }
    if (list.length) {
      url_pipe('tag=' + list.join(',') + '&step=3')
    } else {
      alert($('[data-step=2] .entry-title').text())
    }
  })

  function btnWithReset(context, callback) {
    var t = context.getAttribute('type');
    if (t === 'reset') {
      url_pipe('step=1');
      return;
    } else {
      callback && callback();
    }
  }

  $('[data-step=3] button').click(function(e) {
    btnWithReset(this, function(){
      var ids = [];
      $('.entry-book-list tbody .active').each(function(i, d) {
        ids.push($(d).data('id'));
      });
      if (ids.length) {
        url_pipe('step=4&list='+ids.join(','));
      } else {
        alert('请至少选中一本书籍');
      }
    })
  })

  $('[data-step=4] button').click(function(e) {
    btnWithReset(this, function(){
      if (!$('input[name=mobile]').val().length) {
        e.preventDefault();
        alert('请填写手机号, 方便我们联系')
        return false;
      }
    });
  })

  $('[data-step=0] button').click(function(e) {
    btnWithReset(this);
  })
})
