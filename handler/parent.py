from tornado.web import HTTPError
from wtforms.form import Form
from wtforms.fields import SelectField
from wtforms.fields.html5 import TelField
from wtforms.ext.sqlalchemy.orm import model_form
from model import Parent
from handler.base import Base, FormWrapper
from sqlalchemy import desc


__all__ = ["ParentHandler"]


class ParentHandler(Base):
  def set_form(self):
    form = model_form(Parent, Form, exclude=['created', 'updated'])
    marriage = ['single', 'married', 'divorced', 'widowed']
    gender = ['male', 'female']
    form.gender = SelectField(coerce=int,
        choices= list(enumerate(gender)),
      )
    form.marriage = SelectField(coerce=int,
        choices=list(enumerate(marriage)),
      )
    form.phone = TelField()
    self.form = form

  def put(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      d = self.db.query(Parent).filter_by(id=id).first()
      f.populate_obj(d)
      d.save()
      self.redirect('/parent/' + id)
    else:
      self.set_status(400)
      self.write(f.errors)

  def post(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      Parent(**f.data).save()
      self.redirect('/parent/')
    else:
      self.set_status(400)
      self.write(f.errors)

  def delete(self, id):
    d = self.db.query(Parent).filter_by(id=id).first()
    if d:
      d.delete()
      self.redirect('/parent/')
    else:
      raise HTTPError(404)

  def get(self, id):
    q = self.db.query(Parent).order_by(desc(Parent.created))
    if id:
      q = q.filter_by(id=id)
    #self.render('parent.html', pager=self.get_page_data(q))
    self.render('form.html',
                ft='parent',
                attrs = ['id', 'name', 'age', 'phone'],
                pager=self.get_page_data(q))
