from tornado.web import HTTPError
from wtforms.form import Form
from wtforms.fields.html5 import DateField
from wtforms.ext.sqlalchemy.orm import model_form
from model import History
from handler.base import Base, FormWrapper
from sqlalchemy import desc


__all__ = ["HistoryHandler"]


class HistoryHandler(Base):
  def set_form(self):
    form = model_form(History, base_class=Form, db_session=self.db,
                      exclude=['created', 'updated'])
    form.borrow_time = DateField(format='%Y-%m-%d')
    form.return_time = DateField(format='%Y-%m-%d')
    self.form = form
    self.model = History
    self.ft = 'history'
    self.attrs = ['id', 'user', 'book', 'borrow_time', 'return_time', 'comment'],

  def _put(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      d = self.db.query(History).filter_by(id=id).first()
      f.populate_obj(d)
      d.save()
      self.redirect('/history/' + id)
    else:
      self.set_status(400)
      self.write(f.errors)

  def _post(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      History(**f.data).save()
      self.redirect('/history/')
    else:
      self.set_status(400)
      self.write(f.errors)

  def _delete(self, id):
    d = self.db.query(History).filter_by(id=id).first()
    if d:
      d.delete()
      self.redirect('/history/')
    else:
      raise HTTPError(404)

  def _get(self, id):
    q = self.db.query(History).order_by(desc(History.created))
    if id:
      q = q.filter_by(id=id)
    #self.render('history.html', pager=self.get_page_data(q))
    self.render('form.html',
                ft='history',
                attrs = ['id', 'user', 'book', 'borrow_time', 'return_time', 'comment'],
                pager=self.get_page_data(q))
