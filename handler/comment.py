from tornado.web import HTTPError
from wtforms.form import Form
from wtforms.widgets import TextArea
from wtforms.ext.sqlalchemy.orm import model_form
from model import Comment
from handler.base import Base, FormWrapper
from sqlalchemy import desc


__all__ = ["CommentHandler"]


class CommentHandler(Base):
  def set_form(self):
    self.form = model_form(Comment, Form, exclude=['created', 'updated'],
                           field_args={
                             'awards': {'widget': TextArea()},
                           })

  def put(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      d = self.db.query(Comment).filter_by(id=id).first()
      f.populate_obj(d)
      d.save()
      self.redirect('/comment/' + id)
    else:
      self.set_status(400)
      self.write(f.errors)

  def post(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      Comment(**f.data).save()
      self.redirect('/comment/')
    else:
      self.set_status(400)
      self.write(f.errors)

  def delete(self, id):
    d = self.db.query(Comment).filter_by(id=id).first()
    if d:
      d.delete()
      self.redirect('/comment/')
    else:
      raise HTTPError(404)

  def get(self, id):
    q = self.db.query(Comment).order_by(desc(Comment.created))
    if id:
      q = q.filter_by(id=id)
    #self.render('comment.html', pager=self.get_page_data(q))
    self.render('form.html',
                ft='comment',
                attrs = ['id', 'username', 'content', 'book_id', 'source'],
                pager=self.get_page_data(q))
