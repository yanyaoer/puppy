from itertools import chain
from sqlalchemy import func, or_
from wtforms.form import Form
from wtforms.ext.sqlalchemy.orm import model_form
from model import Book, List, Tag, TagBookLink
from handler.base import Base, FormWrapper


class ChooseHandler(Base):
  def set_form(self):
    self.form = model_form(List, base_class=Form, db_session=self.db,
                           exclude=['created', 'updated'])

  def get_book_result(self):
    ret = []
    step = int(self.get_argument('step', 0))
    age = self.get_argument('age', '')
    tag = self.get_argument('tag', '')
    if step != 3:
      return ret
    if age:
      age_list = set([int(a) for a in age.split(',')])
      tag_list = set([int(a) for a in tag.split(',') if a])
      m = list(getattr(Book, 'age{d}'.format(d=d))==1 for d in age_list)
      f = or_(*m) if len(m) > 1 else m[0]
      fargs = [f, TagBookLink.book_id == Book.id]
      if tag:
        fargs.append(TagBookLink.tag_id.in_(tag_list))
      book_list = set(chain.from_iterable(self.db.query(Book.id)\
          .filter(*fargs).all()))
      ret = self.db.query(Book, func.group_concat(Tag.name.distinct()))\
                  .filter(TagBookLink.book_id == Book.id,
                          TagBookLink.tag_id == Tag.id,
                          Book.id.in_(book_list))\
                  .group_by(Book.id).all()
    return ret

  def get(self):
    step = int(self.get_argument('step', 0))
    bid_list = self.get_argument('list', '').split(',')
    book_output = []
    if step == 4 and bid_list:
      book_output = self.db.query(Book, func.group_concat(Tag.name.distinct()))\
                          .filter(TagBookLink.book_id == Book.id,
                                  TagBookLink.tag_id == Tag.id,
                                  Book.id.in_(bid_list))\
                          .group_by(Book.id).all()
    self.render('choose.html',
                book_list = self.get_book_result(),
                book_output = book_output,
                tag_list = self.db.query(Tag).filter_by(type=1).all())

  def post(self):
    f = self.form(FormWrapper(self))
    if f.validate():
      List(**f.data).save()
      self.redirect('/choose?step=0')
    else:
      self.set_status(400)
      self.write(f.errors)
