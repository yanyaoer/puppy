import os
from tornado import ioloop, locale
from tornado.web import Application
from tornado.options import define, options, parse_command_line
from model import db
import uimodule
import handler


define("debug", True)
define("port", 8000)
parse_command_line()


class app(Application):
  def __init__(self):
    _dir = os.path.dirname(__file__)
    settings = dict(
      static_path=os.path.join(_dir, "static"),
      template_path=os.path.join(_dir, "tpl"),
      ui_modules=uimodule,
      #ui_methods=uimethods,
      cookie_secret='a0BQmppxSH2SFZ0yjUvW8A+TTIL4d0OcowfIiv0OVxI=',
      debug=options.debug,
      gzip=True,
    )
    locale.load_translations(os.path.join(_dir, "lang"))
    locale.set_default_locale('zh_CN')
    Application.__init__(self, router, **settings)
    self.db = db


router = [
  (r"/author/(\d+)?", handler.author.AuthorHandler),
  (r"/book/(\d+)?", handler.book.BookHandler),
  (r"/comment/(\d+)?", handler.comment.CommentHandler),
  (r"/history/(\d+)?", handler.history.HistoryHandler),
  (r"/parent/(\d+)?", handler.parent.ParentHandler),
  (r"/tag/(\d+)?", handler.tag.TagHandler),
  (r"/user/(\d+)?", handler.user.UserHandler),
  (r"/booklist/(\d+)?", handler.list.ListHandler),
  (r"/login/?", handler.api.LoginHandler),
  (r"/api/(\w+)?", handler.api.ApiHandler),
  (r"/choose/?", handler.choose.ChooseHandler),
  (r"/(.*)", handler.base.IndexHandler),
]


def main():
  server = app()
  #if not options.debug:
    #import setting
    #from raven.contrib.tornado import AsyncSentryClient
    #server.sentry_client = AsyncSentryClient(setting.sentry_url)
  server.listen(options.port)
  ioloop.IOLoop.current().start()


if __name__ == "__main__":
  main()
