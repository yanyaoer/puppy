from tornado.web import HTTPError
from wtforms import validators
from wtforms.form import Form
from wtforms.ext.sqlalchemy.orm import model_form
from model import User
from handler.base import Base, FormWrapper
from sqlalchemy import desc


__all__ = ["UserHandler"]


class UserHandler(Base):
  def set_form(self):
    form = model_form(User, base_class=Form, db_session=self.db,
                      exclude=['created', 'updated', 'history_collection'],
                      field_args={
                        'username': {'validators': [
                          validators.Required(),
                          validators.Length(min=5)
                        ]},
                      })
    self.model = User
    self.ft = 'user'
    self.attrs = ['id', 'name', 'username', 'card_id']
    self.form = form

  def _post(self, id):
    f = self.form(FormWrapper(self))
    if f.validate():
      User(**f.data).save()
      self.redirect('/user/')
    else:
      self.set_status(400)
      self.write(f.errors)

  def _get(self, id):
    q = self.db.query(User).order_by(desc(User.created))
    if id:
      q = q.filter_by(id=id)
    #self.render('user.html', pager=self.get_page_data(q))
    self.render('form.html',
                ft='user',
                attrs = ['id', 'name', 'username', 'card_id'],
                pager=self.get_page_data(q))
